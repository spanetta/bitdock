# :coding: utf-8
# :copyright: Copyright (c) 2014 ftrack

import argparse
import logging
import sys
import json

import cherrypy
import requests
from cherrypy.lib.httputil import parse_query_string

class Bitdock(object):
    '''Bitdock main handler.'''

    def __init__(self, flowdock_api_key, user_mapping=None, flow_mapping=None):
        '''Initialise handler with *flowdock_api_key*.
            
        In addition, optionally specify a *user_mapping* to use when mapping
        userns. It should be a dictionary of Bitbucket user names against
        Flowdock user dictionaries. The Flowdock user dictionary should contain
        keys for username, display_name and email.


        '''
        super(Bitdock, self).__init__()
        self.flowdock_api_key = flowdock_api_key
        self.user_mapping = user_mapping or {}
        self.flow_mapping = flow_mapping or {}
        self._flowdock_api_url_base = (
            'https://api.flowdock.com/v1/'
        )
        self._flowdock_api_messages_endpoint = (
            'messages'
        )
        self._flowdock_api_comments_endpoint = (
            'comments'
        )
        self.pull_request_message_ids = {}
        self.commit_source_to_pull_request = {}
        
        self.pull_request_regex = 'pullrequests/(?P<pull_request_id>[^/]*)/'

    def flowdock_api_url(self, api_key=None, is_comment=False):
        '''Return flowdock api url.'''
        api_key = api_key or self.flowdock_api_key
        endpoint = self._flowdock_api_comments_endpoint if is_comment else self._flowdock_api_messages_endpoint
        return '{2}@{0}{1}'.format(self._flowdock_api_url_base, endpoint, api_key)


    @cherrypy.tools.json_in()
    @cherrypy.expose()
    def webhook_pull_request(self, event):
        '''Handle bitbucket pull request notification using new webhooks.'''
        action = event #parse_query_string(cherrypy.request.query_string)['event']
        data = cherrypy.request.json
        
        # Convert author to flowdock user if possible.
        author = self.user_mapping.get(data['actor']['username'])
        if author is None:
            author = {
                'username': data['actor']['username'],
                'display_name': data['actor']['display_name'],
                'email': 'no-reply@bitbucket.org'
            }
        
        # Add tags, including a user tag for each reviewer that can be mapped to
        # a flowdock user.
        tags = ['pull-request']

        reviewers = []
        for reviewer in data['pullrequest'].get('reviewers', []):
            flowdock_user = self.user_mapping.get(reviewer['username'])
            if flowdock_user:
                tags.append('@{0}'.format(flowdock_user['username']))
                reviewers.append(flowdock_user)
            else:
                reviewers.append(reviewer)

        # Construct main message content.
        content = [
            ('respository', data['repository']['name']),
            ('branches', '{0} > {1}'.format(
                data['pullrequest']['source']['branch']['name'],
                data['pullrequest']['destination']['branch']['name']
            )),
            ('author', author['display_name'])
        ]

        if reviewers:
            content.append((
                'reviewers',
                ', '.join([reviewer['display_name'] for reviewer in reviewers])
            ))

        if data['pullrequest']['description']:
            content.append(('description', data['pullrequest']['description']))

        # Contains to a comment
        pr_comment = data.get('comment', {}).get('content', {}).get('raw')
        if pr_comment:
            content.insert(0, ('comment', pr_comment))

        # Declines sometimes have a reason.
        reason = data['pullrequest'].get('reason')
        if reason:
            content.insert(0, ('reason', reason))

        content = '<br/>'.join([
            '<b>{0}: </b>{1}'.format(key.capitalize(), value)
            for key, value in content
        ])

        # Construct payload for Flowdock.
        payload = {
            'source': 'Bitbucket',
            'subject': 'Pull Request {0}: {1}'.format(
                action.capitalize(), data['pullrequest']['title']
            ),
            'external_user_name': author['display_name'],
            'tags': tags,
            'content': content,
            'event': 'message'
        }

        # For some odd reason, only created pull requests include a link to
        # themselves.
        link = data['pullrequest'].get('links', {}).get('html', {}).get('href')
        if link:
            payload['link'] = link.replace('//api.', '//')

        is_comment = False
        
        pull_request_id = data['pullrequest']['id']
        
        if pull_request_id:
            message_id = self.pull_request_message_ids.get(pull_request_id)
            if message_id:
                is_comment = True
                payload['event'] = 'comment'
                payload['message'] = message_id
        
        # Send request.
        response = requests.post(
            self.flowdock_api_url(self.flow_mapping.get(data['pullrequest']['source']['repository']['name']), is_comment),
            headers={'content-type': 'application/json'},
            data=json.dumps(payload)
        )
        
        response_json = response.json()
        
        if not message_id:
            message_id = response_json['id']
            self.pull_request_message_ids[pull_request_id] = message_id
            
        pr_state = data['pullrequest']['state']
        if (pr_state == 'MERGED' | pr_state == 'DECLINED'):
            if self.pull_request_message_ids.get(pull_request_id):
                del self.pull_request_message_ids[pull_request_id]
            
        response.raise_for_status()

    @cherrypy.tools.json_in()
    @cherrypy.expose()
    def bitbucket_pull_request(self):
        '''Handle bitbucket pull request notification.'''
        data = cherrypy.request.json

        if 'pullrequest_created' in data:
            action = 'created'
            data = data['pullrequest_created']

        elif 'pullrequest_merged' in data:
            action = 'merged'
            data = data['pullrequest_merged']

        elif 'pullrequest_declined' in data:
            action = 'declined'
            data = data['pullrequest_declined']

        else:
            raise ValueError('Unrecognised pull request action.')

        # Convert author to flowdock user if possible.
        author = self.user_mapping.get(data['author']['username'])
        if author is None:
            author = {
                'username': data['author']['username'],
                'display_name': data['author']['display_name'],
                'email': 'no-reply@bitbucket.org'
            }

        # Add tags, including a user tag for each reviewer that can be mapped to
        # a flowdock user.
        tags = ['pull-request']

        reviewers = []
        for reviewer in data.get('reviewers', []):
            flowdock_user = self.user_mapping.get(reviewer['username'])
            if flowdock_user:
                tags.append('@{0}'.format(flowdock_user['username']))
                reviewers.append(flowdock_user)
            else:
                reviewers.append(reviewer)

        # Construct main message content.
        content = [
            ('respository', data['source']['repository']['name']),
            ('branches', '{0} > {1}'.format(
                data['source']['branch']['name'],
                data['destination']['branch']['name']
            )),
            ('author', author['display_name'])
        ]

        if reviewers:
            content.append((
                'reviewers',
                ', '.join([reviewer['display_name'] for reviewer in reviewers])
            ))

        if data['description']:
            content.append(('description', data['description']))

        # Declines sometimes have a reason.
        reason = data.get('reason')
        if reason:
            content.insert(0, ('reason', reason))

        content = '<br/>'.join([
            '<b>{0}: </b>{1}'.format(key.capitalize(), value)
            for key, value in content
        ])

        # Construct payload for Flowdock.
        payload = {
            'source': 'Bitbucket',
            'subject': 'Pull Request {0}: {1}'.format(
                action.capitalize(), data['title']
            ),
            'external_user_name': author['display_name'],
            'tags': tags,
            'content': content,
            'event': 'message'
        }

        # For some odd reason, only created pull requests include a link to
        # themselves.
        link = data.get('links', {}).get('html', {}).get('href')
        if link:
            payload['link'] = link.replace('//api.', '//')

        is_comment = False
        commit_hash = data['source']['commit']['hash']
        pull_request_id = 0
        if (action == 'created'):
            pull_request_id = re.search(pull_request_regex, data['links']['self']).groupdict()['pull_request_id']
            commit_source_to_pull_request[commit_hash] = pull_request_id
        else:
            pull_request_id = commit_source_to_pull_request[commit_hash]
        
        if pull_request_id:
            message_id = pull_request_message_ids[pull_request_id]
            if message_id:
                is_comment = True
                payload['event'] = 'comment'
                payload['message'] = message_id
        
        # Send request.
        response = requests.post(
            self.flowdock_api_url(self.flow_mapping.get(data['source']['repository']['name']), is_comment),
            headers={'content-type': 'application/json'},
            data=json.dumps(payload)
        )
        
        response_json = response.json()
        
        if (action == 'created'):
            message_id = response_json['id']
            pull_request_message_ids[pull_request_id] = message_id
            
        if (action == 'merged' | action == 'declined'):
            del pull_request_message_ids[pull_request_id]
            
        response.raise_for_status()
    
    
    @cherrypy.tools.json_in()
    @cherrypy.expose()
    def bitbucket_comment(self):
        '''Handle bitbucket pull request notification.'''
        data = cherrypy.request.json

        if 'pullrequest_comment_created' in data:
            action = 'comment created'
            data = data['pullrequest_comment_created']

        elif 'pullrequest_comment_updated' in data:
            action = 'comment updated'
            data = data['pullrequest_comment_updated']

        elif 'pullrequest_comment_deleted' in data:
            action = 'comment deleted'
            data = data['pullrequest_comment_deleted']

        else:
            raise ValueError('Unrecognised pull request action.')

        # Convert author to flowdock user if possible.
        author = self.user_mapping.get(data['user']['username'])
        if author is None:
            author = {
                'username': data['user']['username'],
                'display_name': data['user']['display_name'],
                'email': 'no-reply@bitbucket.org'
            }

        # Add tags, including a user tag for each reviewer that can be mapped to
        # a flowdock user.
        tags = ['pull-request', 'pull-request-comment']

        # Construct main message content.
        content = [
            ('raw content', data['content']['raw']),
            ('link', data['links']['html']['href']),
            ('path', data['inline']['path']),
            ('author', author['display_name'])
        ]

        content = '<br/>'.join([
            '<b>{0}: </b>{1}'.format(key.capitalize(), value)
            for key, value in content
        ])

        # Construct payload for Flowdock.
        payload = {
            'source': 'Bitbucket',
            'subject': 'Pull Request {0}: {1}'.format(
                action.capitalize(), author['display_name']
            ),
            'external_user_name': author['display_name'],
            'tags': tags,
            'content': content,
            'event': 'message'
        }

        payload['link'] = data['links']['html']['href']
        
        is_comment=False
        pull_request_id = re.search(pull_request_regex, data['links']['self']).groupdict()['pull_request_id']
        if pull_request_id:
            message_id = pull_request_message_ids[pull_request_id]
            if message_id:
                is_comment = True
                payload['event'] = 'comment'
                payload['message'] = message_id

        # Send request.
        response = requests.post(
            self.flowdock_api_url(self.flow_mapping.get(data['source']['repository']['name']), is_comment),
            headers={'content-type': 'application/json'},
            data=json.dumps(payload)
        )
        response.raise_for_status()
            
    def _get_flowdock_user(self, bitbucket_user):
        '''Return flowdock user for *bitbucket_user*.'''
        user = self.user_mapping.get(bitbucket_user['username'])
        if user is None:
            user = {
                'username': bitbucket_user['username'],
                'display_name': bitbucket_user['display_name'],
                'email': 'no-reply@bitbucket.org'
            }
        return user


def main(arguments=None):
    '''Bitdock: Connect Bitbucket and Flowdock.'''
    if arguments is None:
        arguments = []

    parser = argparse.ArgumentParser('bitdock')
    parser.add_argument(
        'flowdock_api_key',
        help='The API key to use for communicating with Flowdock.'
    )

    parser.add_argument(
        '--host', help='Host interface to run on.', default='0.0.0.0'
    )

    parser.add_argument(
        '--port', help='Host port to run on.', type=int, default=9000
    )

    parser.add_argument(
        '--user-mapping',
        help='Optional path to a user mapping json file. The contents should '
             'in the form {"bitbucket_username": '
             '{username: "flowdock_username", '
             '"display_name": "flowdock_display_name", '
             '"email": "email_address"}}'
    )
    parser.add_argument(
        '--flow-mapping',
        help='Optional path to a flow mapping json file. The contents should '
             'in the form {"bitbucket_flowname": '
             '{flowname: "flowdock_flowname", '
             '"display_name": "flowdock_display_name", '
             '"email": "email_address"}}'
    )

    # Allow setting of logging level from arguments.
    loggingLevels = {}
    for level in (
        logging.NOTSET, logging.DEBUG, logging.INFO, logging.WARNING,
        logging.ERROR, logging.CRITICAL
    ):
        loggingLevels[logging.getLevelName(level).lower()] = level

    parser.add_argument(
        '-v', '--verbosity',
        help='Set the logging output verbosity.',
        choices=loggingLevels.keys(),
        default='info'
    )

    namespace = parser.parse_args(arguments)

    logging.basicConfig(level=loggingLevels[namespace.verbosity])
    log = logging.getLogger('bitdock.main')

    # Read user map.
    user_mapping = None
    if namespace.user_mapping is not None:
        with open(namespace.user_mapping, 'r') as file_descriptor:
            user_mapping = json.load(file_descriptor)
    
    flow_mapping = None
    if namespace.flow_mapping is not None:
        with open(namespace.flow_mapping, 'r') as file_descriptor:
            flow_mapping = json.load(file_descriptor)

    # Construct and start application.
    application = Bitdock(
        flowdock_api_key=namespace.flowdock_api_key,
        user_mapping=user_mapping,
        flow_mapping=flow_mapping
    )

    cherrypy.config.update({
        'server.socket_host': namespace.host,
        'server.socket_port': namespace.port
    })
    cherrypy.quickstart(application)


if __name__ == '__main__':
    raise SystemExit(main(sys.argv[1:]))
